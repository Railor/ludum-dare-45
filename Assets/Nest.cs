﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nest : MonoBehaviour, IHasPlayer
{
    private int level = 1;

    public Player player;
    public List<Ant> gathererChildren;
    public List<Ant> fighterChildren;
    private int foodCollected = 0;
    private int totalFoodCollected = 0;
    private int fighterTime = 0;
    private int gatherCounter = 100;
    public float gatherRange = 5f;

    // Start is called before the first frame update
    void Start()
    {
        this.gathererChildren = new List<Ant>();
        this.fighterChildren = new List<Ant>();
        createNewGatherer();
        createNewFighter();
    }

    public void createNewGatherer()
    {
        Ant ant = GameManager.createAnt(this);
        ant.setJob(new Gatherer());
        ant.gameObject.transform.localScale *= .5f;
        this.gathererChildren.Add(ant);
    }

    public void createNewFighter()
    {
        Ant ant = GameManager.createAnt(this);
        ant.setJob(new FighterJob());
        this.fighterChildren.Add(ant);
    }

    public void onCollectFood(int amount)
    {
        foodCollected += amount;
        totalFoodCollected += amount;

        if (foodCollected > level * 20)
        {
            foodCollected = 0;
            level++;

            gatherRange += 1;

            if (level > 10)
            {
                level = 10;
            }
            
            this.transform.localScale = new Vector3(1 + (level * .25f), 1 + (level * .25f), 1);
        }

//        fighterTime++;
//
//        if (fighterTime > Mathf.Max(30 - (level * GameManager.difficulty), 10))
//        {
//            fighterTime = 0;
//            if (player != GameManager.getInstance().myPlayer)
//            {
//                createNewFighter();
//            }
//        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gathererChildren.Count == 0)
        {
            gatherCounter--;

            if (gatherCounter <= 0)
            {
                gatherCounter = 100;
                createNewGatherer();
            }
        }
    }

    private void OnDestroy()
    {
        player.danger += 600;
        player.nests.Remove(this);
        foreach (var ant in gathererChildren)
        {
            if (ant != null && ant.enabled)
                Destroy(ant.gameObject);
        }

        foreach (var ant in fighterChildren)
        {
            if (ant != null && ant.enabled)
                Destroy(ant.gameObject);
        }
    }

    public Player getPlayer()
    {
        return player;
    }

    public int getWorkerCost()
    {
        return gathererChildren.Count * 10 + 5;
    }
}