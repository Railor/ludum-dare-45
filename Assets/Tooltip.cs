﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    private Camera main;

    public GameObject container;

    public TextMeshProUGUI workerText;
    public TextMeshProUGUI warriorText;
    // Start is called before the first frame update
    void Start()
    {
        main = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePos = main.ScreenToWorldPoint(Input.mousePosition);
        Player myPlayer = GameManager.getInstance().myPlayer;
        Nest aliveNest = TargetHelper.getNearestOfTypePlayer<Nest>(mousePos, .25f, myPlayer);

        if (aliveNest != null)
        {
            show();
            workerText.text = aliveNest.getWorkerCost().ToString();
            workerText.color = myPlayer.getTotalFood() >= aliveNest.getWorkerCost() ? Color.green : Color.red;
            warriorText.color = myPlayer.getTotalFood() >= 25 ? Color.green : Color.red;
            Vector2 screenPos = main.WorldToScreenPoint(aliveNest.transform.position);
            transform.position = new Vector2(screenPos.x, screenPos.y + 125);
        }
        else
        {
            hide();
        }
    }

    public void show()
    {
        container.SetActive(true);
    }

    public void hide()
    {
        container.SetActive(false);
    }
}
