﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_UpgradePanel : MonoBehaviour
{
    public GameObject mainContainer;
    public GameObject hiddenPanel;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI damageCost;
    
    public TextMeshProUGUI armorText;
    public TextMeshProUGUI armorCost;
    
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI healthCost;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Player player = GameManager.getInstance().myPlayer;

        damageText.text = "Damage: " + player.damage;
        armorText.text = "Armor: " + player.armor;
        healthText.text = "Health: " + player.health;

        damageCost.text = player.damageCost.ToString();
        armorCost.text = player.armorCost.ToString();
        healthCost.text = player.healthCost.ToString();
    }

    public void toggleVisibility()
    {
        mainContainer.SetActive(!mainContainer.activeSelf);
        hiddenPanel.SetActive(!mainContainer.activeSelf);
    }

    public void upgradeDamage()
    {
        GameManager.getInstance().myPlayer.buyDamageUpgrade();
    }
    
    public void upgradeArmor()
    {
        GameManager.getInstance().myPlayer.buyArmorUpgrade();
    }
    
    public void upgradeHealth()
    {
        GameManager.getInstance().myPlayer.buyHealthUpgrade();
    }
}
