﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_FOOD : MonoBehaviour
{
    public TextMeshProUGUI foodText;
    public TextMeshProUGUI nestText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Player player = GameManager.getInstance().myPlayer;
        foodText.text = player.getTotalFood().ToString();
        nestText.text = player.getNestCost().ToString();
        if (player.getTotalFood() >= player.getNestCost())
        {
            nestText.color = Color.green;
        }
        else
        {
            nestText.color = Color.red;
        }
    }
}
