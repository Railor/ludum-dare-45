using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Gatherer : IAntJob
{
    private Food targetFood;
    private Vector2 foodPos;
    private GatherState _gatherState;
    private int munchTime = 0;
    private int foodTrips = 0;
    
    public void execute(Ant ant)
    {
        if (_gatherState == GatherState.SEARCHING)
        {
            if (ant.getHoldingFood() > 0)
            {
                _gatherState = GatherState.DEPOSITING;
                return;
            }

            if (foodTrips <= 0 || targetFood == null || targetFood.getFood() == 0)
            {
                targetFood = TargetHelper.getNearestFood(ant.transform.position, ant.nest.gatherRange);
                foodTrips = 5;
            }
            else
            {
                foodTrips--;
            }

            if (targetFood != null)
            {
                _gatherState = GatherState.COLLECTING;
                munchTime = 0;
                foodPos = new Vector2(targetFood.transform.position.x + Random.Range(-.25f,.25f), targetFood.transform.position.y + Random.Range(-.25f,.25f));
            }
        }

        if (_gatherState == GatherState.COLLECTING)
        {
            if (targetFood == null)
            {
                _gatherState = GatherState.SEARCHING;
                return;
            }

            if (targetFood.getFood() == 0)
            {
                _gatherState = GatherState.SEARCHING;
                return;
            }

            if (Vector2.Distance(ant.transform.position, foodPos) - .5f < (targetFood.getFood() * .2f) * .016f)
            {
                munchTime += 1;
                if (munchTime < 60)
                {
                    
//                    ant.navMeshAgent.isStopped = true;
                    return;
                }

                munchTime = 0;
                if (targetFood.getFood() < ant.carryCapacity)
                {
                    ant.setHoldingFood(targetFood.getFood());
                }
                else
                {
                    ant.setHoldingFood(ant.carryCapacity);
                }

                targetFood.removeFood(ant.getHoldingFood());
                _gatherState = GatherState.DEPOSITING;
            }
            else
            {
//                ant.navMeshAgent.SetDestination(targetFood.transform.position);
//                ant.navMeshAgent.isStopped = false;
                ant.transform.position = Vector2.MoveTowards(ant.transform.position, foodPos, Time.deltaTime * ant.speed);
                ant.transform.up = Vector3.Lerp(ant.transform.up, (foodPos - (Vector2)ant.transform.position), .05f);
            }
        }

        if (_gatherState == GatherState.DEPOSITING)
        {
//            ant.navMeshAgent.SetDestination(foodPos);
//            ant.navMeshAgent.isStopped = false;
            ant.transform.position = Vector2.MoveTowards(ant.transform.position, ant.nest.transform.position, Time.deltaTime * ant.speed);
            ant.transform.up = Vector3.Lerp(ant.transform.up, (ant.nest.transform.position - ant.transform.position), .05f);
            
            if (Vector2.Distance(ant.transform.position, ant.nest.transform.position) < .5f)
            {
                ant.nest.onCollectFood(ant.getHoldingFood());
                ant.player.addFood(ant.getHoldingFood());
                ant.setHoldingFood(0);
                _gatherState = GatherState.SEARCHING;
//                ant.navMeshAgent.isStopped = true;

            }
        }

    }

    public void cancel(Ant ant)
    {
   
    }

    private enum GatherState
    {
        SEARCHING,
        COLLECTING,
        DEPOSITING
    }
}
