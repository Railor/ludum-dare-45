public interface IAntJob
{
    void execute(Ant ant);
    void cancel(Ant ant);
}
