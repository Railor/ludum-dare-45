using System;
using UnityEngine;

public class QueenJob : IAntJob
{
    private Vector2 targetPosition;

    public QueenJob(Vector2 targetPosition)
    {
        this.targetPosition = targetPosition;
    }

    public void execute(Ant ant)
    {
        ant.transform.position = Vector2.MoveTowards(ant.transform.position, targetPosition, Time.deltaTime * ant.speed);
        ant.transform.up = Vector3.Lerp(ant.transform.up, (targetPosition - (Vector2)ant.transform.position), .05f);
        
        Debug.DrawLine(ant.transform.position, targetPosition, ant.player.color);
        
        if (Vector2.Distance(ant.transform.position, targetPosition) < .35f)
        {
            GameManager.createNest(targetPosition, ant.player);
            ant.player.queens.Remove(ant.gameObject);
            GameObject.Destroy(ant.gameObject);
        }
    }

    public void cancel(Ant ant)
    {
        ant.player.queens.Remove(ant.gameObject);
    }
}