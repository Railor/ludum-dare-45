using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class FighterJob : IAntJob
{
    private GameObject target;
    private Vector2 targetPosition;
    private int targetFinder = 0;
    private int attackCooldown = 0;
    private int wanderTimer = 9999;
    private int flagFinder = 0;
    private Flag flag;

    public FighterJob()
    {
    }

    public void execute(Ant ant)
    {
        attackCooldown -= 1;
        if (attackCooldown < 0)
        {
            attackCooldown = 0;
        }

        if (targetFinder <= 0)
        {
            target = TargetHelper.getNearestEnemy(ant.transform.position, 5, ant.player);
            targetFinder = 5;
        }
        else
        {
            targetFinder--;
        }

        if (target != null)
        {
            targetPosition = target.transform.position;
            ant.transform.up = Vector3.Lerp(ant.transform.up, (targetPosition - (Vector2) ant.transform.position), .05f);


            if (Vector2.Distance(ant.transform.position, targetPosition) < .5f)
            {
                if (attackCooldown <= 0)
                {
                    attackCooldown = 30;
                    Health health = target.gameObject.GetComponent<Health>();
                    health.takeDamage(ant.player.damage);
                }
            }
            else
            {
                ant.transform.position = Vector2.MoveTowards(ant.transform.position, targetPosition, Time.deltaTime * ant.speed);
            }

            return;
        }

        if (flagFinder <= 0)
        {
            flag = TargetHelper.getNearestFlag(ant.player, ant.transform.position);
            flagFinder = 120;
        }
        else
        {
            flagFinder -= 1;
        }
        
        if (flag == null)
        {
            flag = null;
            wanderTimer++;

            if (wanderTimer > 600)
            {
                wanderTimer = 0;
                targetPosition = new Vector2(Random.Range(-5f, 5f) + ant.nest.transform.position.x, Random.Range(-5f, 5f) + ant.nest.transform.position.y);
            }
            ant.transform.position = Vector2.MoveTowards(ant.transform.position, targetPosition, Time.deltaTime * ant.speed);
            ant.transform.up = Vector3.Lerp(ant.transform.up, (targetPosition - (Vector2) ant.transform.position), .05f);
        }
        else
        {
            targetPosition = flag.transform.position;
            if (Vector2.Distance(ant.transform.position, targetPosition) < .5f)
            {
                flag.destroy();
            }

            ant.transform.position = Vector2.MoveTowards(ant.transform.position, targetPosition, Time.deltaTime * ant.speed);
            ant.transform.up = Vector3.Lerp(ant.transform.up, (targetPosition - (Vector2) ant.transform.position), .05f);
        }
    }

    public void cancel(Ant ant)
    {
    }
}