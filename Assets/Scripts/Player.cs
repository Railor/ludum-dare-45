using System.Collections.Generic;
using UnityEngine;

public class Player
{
    private int totalFood;
    public List<Nest> nests;
    public List<Flag> flags;
    public List<GameObject> queens;
    public int queenDelay;
    public Color color;
    public float maxExpandRange = 7;
    public int danger = 0;

    public int damage = 5;
    public int damageCost = 50;

    public int armor;
    public int armorCost = 50;

    public int health = 50;
    public int healthCost = 50;

    public Player(Color color)
    {
        this.color = color;    
        flags = new List<Flag>();
        nests = new List<Nest>();
        queens = new List<GameObject>();
        totalFood = 25;
        queenDelay = Random.Range(450, 1200);
    }

    public int getTotalFood()
    {
        return totalFood;
    }

    public void addFood(int food)
    {
        this.totalFood += food;
    }

    public void addFlag(Vector2 position, int lifeTime = -1, float range = -1)
    {
        this.flags.Add(GameManager.createFlag(position, this, lifeTime, range));
    }

    public void clearFlags()
    {
        foreach (Flag flag in flags)
        {
            GameObject.Destroy(flag.gameObject);
        }
        
        flags.Clear();
    }

    public void buyDamageUpgrade()
    {
        if (getTotalFood() >= damageCost)
        {
            addFood(-damageCost);
            damage++;
            damageCost += 10;
        }
    }
    
    public void buyArmorUpgrade()
    {
        if (getTotalFood() >= armorCost)
        {
            addFood(-armorCost);
            armor++;
            armorCost += 10;
        }
    }
    
    public void buyHealthUpgrade()
    {
        if (getTotalFood() >= healthCost)
        {
            addFood(-healthCost);
            health+= 10;
            healthCost += 10;
        }
    }

    public int getNestCost()
    {
        return ((nests.Count + queens.Count) * 10) + 15;
    }
}
