﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [HideInInspector] public int health;
    public int maxHealth;
    public Player player;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public int getHealth()
    {
        return health;
    }

    public void takeDamage(int amount)
    {
        amount -= player.armor;

        if (amount <= 0)
        {
            amount = 1;
        }

        this.health -= amount;

        if (health <= 0)
        {
            if (GetComponent<Nest>())
            {
                GetComponent<IHasPlayer>().getPlayer().addFlag(transform.position, 1200, 35f);
            }
            else
            {
                if (Random.Range(0f, 1f) > .92f)
                {
                    Player p = GetComponent<IHasPlayer>().getPlayer();
                    if (p != GameManager.getInstance().myPlayer)
                    {
                        GetComponent<IHasPlayer>().getPlayer().addFlag(transform.position, 600, 12f);
                    }

                }
            }
            
            Destroy(this.gameObject);
        }
    }
}