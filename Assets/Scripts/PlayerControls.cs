using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerControls : MonoBehaviour
{
    private Camera main;
    public float cameraSpeed = 3f;

    private void Awake()
    {
        this.main = Camera.main;
    }

    private void Update()
    {
        Player myPlayer = GameManager.getInstance().myPlayer;

        if (Input.GetKeyDown(KeyCode.E))
        {
            myPlayer.addFood(250);
        }

        float mouseScroll = Input.GetAxis("Mouse ScrollWheel");

        if (mouseScroll < 0)
        {
            main.orthographicSize += 1;
        }
        else if (mouseScroll > 0)
        {
            main.orthographicSize -= 1;
        }

        Vector2 mousePos = main.ScreenToWorldPoint(Input.mousePosition);

        float leftRight = Input.GetAxis("Horizontal");
        float upDown = Input.GetAxis("Vertical");

        main.transform.position = new Vector3(main.transform.position.x + leftRight * cameraSpeed * Time.deltaTime,
            main.transform.position.y + upDown * cameraSpeed * Time.deltaTime, -10);

        main.orthographicSize = Mathf.Clamp(main.orthographicSize, 2, 25);
        if (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0))
        {
            Nest aliveNest = TargetHelper.getNearestOfTypePlayer<Nest>(mousePos, .25f, myPlayer);

            if (aliveNest != null)
            {
                if (myPlayer.getTotalFood() >= aliveNest.getWorkerCost())
                {
                    myPlayer.addFood(-(aliveNest.getWorkerCost()));
                    aliveNest.createNewGatherer();
                }
                else
                {
                    FloatingTextManager.getInstance().createFloatingText(mousePos, "Not enough food for Gatherer", Color.red);
                }
            }
            else
            {
                if (myPlayer.getTotalFood() >= myPlayer.getNestCost())
                {
                    if (TargetHelper.getNearestOfType<Food>(mousePos, 1f) != null)
                    {
                        FloatingTextManager.getInstance().createFloatingText(mousePos, "Too close to food", Color.red);
                    }
                    else
                    {
                        Nest nest = TargetHelper.getNearestOfTypePlayer<Nest>(mousePos, 999, myPlayer);

                        if (nest != null)
                        {
                            GameManager.createQueen(nest, mousePos);
                            myPlayer.addFood(-myPlayer.getNestCost());
                        }
                    }
                }
                else
                {
                    FloatingTextManager.getInstance().createFloatingText(mousePos, "Not enough food " + myPlayer.getTotalFood() + "/" + myPlayer.getNestCost(), Color.red);
                }
            }
        }

        if (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(1))
        {
            Nest aliveNest = TargetHelper.getNearestOfTypePlayer<Nest>(mousePos, .25f, myPlayer);

            if (aliveNest != null)
            {
                if (myPlayer.getTotalFood() >= 25)
                {
                    myPlayer.addFood(-25);
                    aliveNest.createNewFighter();
                }
                else
                {
                    FloatingTextManager.getInstance().createFloatingText(mousePos, "Not enough food for Fighter", Color.red);
                }
            }
            else
            {
                Flag flag = TargetHelper.getNearestFlag(myPlayer, mousePos);

                if (flag != null && Vector2.Distance(mousePos, flag.transform.position) < 1f)
                {
                    flag.destroy();
                }
                else
                {
                    myPlayer.addFlag(mousePos);
                }
            }
        }
//
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Player p = GameManager.getInstance().createPlayer();
            GameManager.createNest(mousePos, p);
        }
    }
}