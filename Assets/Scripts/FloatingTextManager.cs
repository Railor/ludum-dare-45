using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloatingTextManager : MonoBehaviour
{
    public GameObject floatingTextPrefab;
    public GameObject floatingContainer;

    private List<FloatingTextData> activeTexts;
    private List<FloatingTextData> inactiveTexts;
    private int lastTick;
    private static FloatingTextManager _instance;

    private void Awake()
    {
        activeTexts = new List<FloatingTextData>();
        inactiveTexts = new List<FloatingTextData>();
        _instance = this;
    }

    public void Update()
    {
        for (int i = 0; i < activeTexts.Count; i++)
        {
            activeTexts[i].lifeTime += 1;
//            if (activeTexts[i].lifeTime > 15)
//            {
//                activeTexts[i].gameObject.transform.localScale *= .95f;
//            }

            if (activeTexts[i].lifeTime > 60)
            {
                activeTexts[i].gameObject.SetActive(false);
                inactiveTexts.Add(activeTexts[i]);
                activeTexts.RemoveAt(i);
                i--;
            }
        }
    }

    public void createFloatingText(Vector2 position, string text, Color color)
    {
        if (inactiveTexts.Count == 0)
        {
            for (int i = 0; i < 5; i++)
            {
                SpawnText();
            }
        }

        FloatingTextData floatingTextData = inactiveTexts[0];
        inactiveTexts.RemoveAt(0);

        floatingTextData.gameObject.SetActive(true);
        floatingTextData.text.text = text;
        floatingTextData.text.color = color;
        floatingTextData.gameObject.transform.position = Camera.main.WorldToScreenPoint(new Vector3(position.x, position.y + .5f, 3));
        floatingTextData.gameObject.transform.localScale = Vector3.one;
        floatingTextData.lifeTime = 0;

        activeTexts.Add(floatingTextData);
    }

    private void SpawnText()
    {
        GameObject go = Instantiate(floatingTextPrefab, floatingContainer.transform);
        TextMeshProUGUI textMeshPro = go.GetComponent<TextMeshProUGUI>();
        inactiveTexts.Add(new FloatingTextData
        {
            gameObject = go, text = textMeshPro
        });
        go.SetActive(false);
    }

    private class FloatingTextData
    {
        public int lifeTime;
        public GameObject gameObject;
        public TextMeshProUGUI text;
    }

    public static FloatingTextManager getInstance()
    {
        return _instance;
    }
}