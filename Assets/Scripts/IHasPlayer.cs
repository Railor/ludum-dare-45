    public interface IHasPlayer
    {
        Player getPlayer();
    }
