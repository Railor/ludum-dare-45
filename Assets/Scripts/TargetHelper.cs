using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TargetHelper
{
    private static LayerMask foodMask;
    private static LayerMask antMask;
    private static LayerMask nestMask;
    private static LayerMask antAndNest;

    public static T getNearestOfType<T>(Vector2 position, float range) where T : MonoBehaviour
    {
        return getNearest<T>(position, getWithinRange<T>(position, range));
    }
    
    public static GameObject getNearestEnemy(Vector2 position, float range, Player player)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, range, antAndNest);
        List<GameObject> things = new List<GameObject>();
        foreach (Collider2D collider in colliders)
        {
            var hasPlayer = collider.gameObject.GetComponent<IHasPlayer>();
            if (hasPlayer.getPlayer() == player)
            {
                continue;
            }

            things.Add(collider.gameObject);
        }
        
        GameObject g = null;
        float minDist = Mathf.Infinity;

        foreach (var go in things)
        {
            float dist = Vector2.Distance(position, go.transform.position);
            if (dist < minDist)
            {
                g = go;
                minDist = dist;
            }
        }

        return g;
    }
    
    public static T getNearestOfTypeNotPlayer<T>(Vector2 position, float range, Player player) where T : MonoBehaviour, IHasPlayer
    {
        return getNearest<T>(position, getWithinRangeNotPlayer<T>(position, range, player));
    }
    
    public static T getNearestOfTypePlayer<T>(Vector2 position, float range, Player player) where T : MonoBehaviour, IHasPlayer
    {
        return getNearest<T>(position, getWithinRangePlayer<T>(position, range, player));
    }
    
    public static Flag getNearestFlag(Player p, Vector2 position)
    {
        List<Flag> flags = p.flags;
    
        Flag g = null;
        float minDist = Mathf.Infinity;

        foreach (var go in flags)
        {
            if (go.player != p)
            {
                continue;
            } 
            
            float dist = Vector2.Distance(position, go.transform.position);
            if (dist < minDist)
            {
                if (go.range > 0)
                {
                    if (dist < go.range)
                    {
                        g = go;
                        minDist = dist;
                    }
                }
                else
                {
                    g = go;
                    minDist = dist;
                }


            }
        }

        return g;
    }
    
    public static Food getNearestEnemy(Vector2 position, float range)
    {
        List<Food> foods = getWithinRange<Food>(position, range);
        Food g = null;
        float minDist = Mathf.Infinity;

        foreach (var go in foods)
        {
            if (go.getFood() <= 0)
            {
                continue;
            }
            
            float dist = Vector2.Distance(position, go.transform.position);
            if (dist < minDist)
            {
                g = go;
                minDist = dist;
            }
        }

        return g;
    }

    public static Food getNearestFood(Vector2 position, float range)
    {
        List<Food> foods = getWithinRange<Food>(position, range);
        Food g = null;
        float minDist = Mathf.Infinity;

        if (foods.Count == 0)
        {
            return null;
        } else if (foods.Count == 1)
        {
            return foods[0];
        }

//        return foods[Random.Range(0, foods.Count - 1)];

        foreach (var go in foods)
        {
            if (go.getFood() <= 0)
            {
                continue;
            }
            
            float dist = Vector2.Distance(position, go.transform.position);
            if (dist + Random.Range(-2f, 5f) < minDist)
            {
                g = go;
                minDist = dist;
            }
        }

        return g;
    }

    public static List<T> getWithinRange<T>(Vector2 position, float range) where T : MonoBehaviour
    {
        LayerMask layerMask;
        if (typeof(T) == typeof(Food))
        {
            layerMask = foodMask;
        }
        else if (typeof(T) == typeof(Ant))
        {
            layerMask = antMask;
        }
        else if (typeof(T) == typeof(Nest))
        {
            layerMask = nestMask;
        }
        else
        {
            return new List<T>();
        }


        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, range, layerMask.value);
        List<T> things = new List<T>();
        foreach (Collider2D collider in colliders)
        {
            things.Add(collider.GetComponent<T>());
        }

        return things;
    }
    
    public static List<T> getWithinRangeNotPlayer<T>(Vector2 position, float range, Player player) where T : MonoBehaviour, IHasPlayer
    {
        LayerMask layerMask;
        if (typeof(T) == typeof(Food))
        {
            layerMask = foodMask;
        }
        else if (typeof(T) == typeof(Ant))
        {
            layerMask = antMask;
        }
        else if (typeof(T) == typeof(Nest))
        {
            layerMask = nestMask;
        }
        else
        {
            return new List<T>();
        }


        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, range, layerMask.value);
        List<T> things = new List<T>();
        foreach (Collider2D collider in colliders)
        {
            var hasPlayer = collider.gameObject.GetComponent<IHasPlayer>();
            if (hasPlayer.getPlayer() == player)
            {
                continue;
            }

            things.Add(collider.GetComponent<T>());
        }

        return things;
    }
    
    public static List<GameObject> getWithinRangeNotPlayer(Vector2 position, float range, Player player, LayerMask layerMask)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, range, layerMask.value);
        List<GameObject> things = new List<GameObject>();
        foreach (Collider2D collider in colliders)
        {
            var hasPlayer = collider.gameObject.GetComponent<IHasPlayer>();
            if (hasPlayer == null && hasPlayer.getPlayer() == player)
            {
                continue;
            }

            things.Add(collider.gameObject);
        }

        return things;
    }
    
    public static List<T> getWithinRangePlayer<T>(Vector2 position, float range, Player player) where T : MonoBehaviour, IHasPlayer
    {
        LayerMask layerMask;
        if (typeof(T) == typeof(Food))
        {
            layerMask = foodMask;
        }
        else if (typeof(T) == typeof(Ant))
        {
            layerMask = antMask;
        }
        else if (typeof(T) == typeof(Nest))
        {
            layerMask = nestMask;
        }
        else
        {
            return new List<T>();
        }


        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, range, layerMask.value);
        List<T> things = new List<T>();
        foreach (Collider2D collider in colliders)
        {
            var hasPlayer = collider.gameObject.GetComponent<IHasPlayer>();
            if (hasPlayer.getPlayer() != player)
            {
                continue;
            }

            things.Add(collider.GetComponent<T>());
        }

        return things;
    }

    public static T getNearest<T>(Vector2 position, List<T> gameObjects) where T : MonoBehaviour
    {
        T g = null;
        float minDist = Mathf.Infinity;

        foreach (var go in gameObjects)
        {
            float dist = Vector2.Distance(position, go.transform.position);
            if (dist < minDist)
            {
                g = go;
                minDist = dist;
            }
        }

        return g;
    }

    static TargetHelper()
    {
        foodMask = LayerMask.GetMask("Food");
        antMask = LayerMask.GetMask("Ant");
        nestMask = LayerMask.GetMask("Nest");
        antAndNest = antMask | nestMask;
    }
}