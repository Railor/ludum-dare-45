﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ant : MonoBehaviour, IHasPlayer
{
    public SpriteRenderer _spriteRenderer;
    public float speed;
    public GameObject myFood;
    private int holdingFood = 0;
    public int carryCapacity = 1;
//    public NavMeshAgent navMeshAgent;

    public Nest nest;
    public Player player;
    private IAntJob job;

    private Vector2 lastPos = new Vector2();

    public Animator animator;

    private int animationId;
    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        animationId = Animator.StringToHash("moving");
    }

    // Update is called once per frame
    void Update()
    {
        job?.execute(this);

        
    }

    private void FixedUpdate()
    {
        if (Vector2.Distance(transform.position, lastPos) > .0001f)
        {
            animator.SetBool(animationId, true);
        }
        else
        {
            animator.SetBool(animationId, false);
        }

        lastPos = transform.position;
    }

    public void setJob(IAntJob antJob)
    {
        if (job != null)
        {
            job?.cancel(this);
            job = null;
        }

        job = antJob;
    }

    public void setHoldingFood(int amount)
    {
        this.holdingFood = amount;
        if (holdingFood == 0)
        {
            myFood.SetActive(false);
        }
        else
        {
            myFood.SetActive(true);
        }
    }

    public int getHoldingFood()
    {
        return holdingFood;
    }

    private void OnDestroy()
    {
        player.danger += 60;
        job?.cancel(this);
    }

    public Player getPlayer()
    {
        return player;
    }
}
