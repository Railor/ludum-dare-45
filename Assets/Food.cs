﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField]
    private int amount;

    private int max;

    private int counter;

    public CircleCollider2D myCollider;
    // Start is called before the first frame update
    void Start()
    {
        this.myCollider = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        counter++;

        if (counter > 2200)
        {
            removeFood(-Random.Range(max/3, max));
            counter = 0;
        }
    }

    public void setFood(int newFoodMx)
    {
        this.max = newFoodMx;
        this.amount = newFoodMx;
        this.transform.localScale = new Vector3(amount * .05f, amount * .05f, 1);
    }

    public int getFood()
    {
        return amount;
    }

    public void removeFood(int foodAmount)
    {
        this.amount -= foodAmount;
        if (getFood() > max)
        {
            amount = max;
        }
        

        if (amount <= 0)
        {
//            Destroy(this.gameObject);
        }
        else
        {
            this.transform.localScale = new Vector3(amount * .05f, amount * .05f, 1);
//            this.myCollider.radius = amount * .05f + .1f;
        }
    }
}
