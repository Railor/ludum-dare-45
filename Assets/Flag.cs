﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour, IHasPlayer
{
    public Player player;
    public SpriteRenderer spriteRenderer;
    public float range;
    public int lifetime = -1;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lifetime != -1)
        {
            lifetime--;

            if (lifetime <= 0)
            {
                destroy();
            }
        }
    }

    public Player getPlayer()
    {
        return player;
    }

    public void destroy()
    {
        this.player.flags.Remove(this);
        Destroy(this.gameObject);
    }
}
