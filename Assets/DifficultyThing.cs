﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DifficultyThing : MonoBehaviour
{
    public TextMeshProUGUI TextMeshProUgui;
    public Slider slider;
    
    public TextMeshProUGUI sizeText;
    public Slider sizeSlider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TextMeshProUgui.text = "Difficulty: " + Mathf.Round(slider.value * 100) + "%";
        sizeText.text = "Size: " + sizeSlider.value;

    }

    public void StartGame()
    {
        GameManager.difficulty = slider.value;
        GameManager.mapSize = (int)sizeSlider.value;
        SceneManager.LoadScene("Game");
    }
}
