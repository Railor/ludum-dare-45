﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpPanel : MonoBehaviour
{
    public GameObject mainContainer;
    public GameObject hiddenPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void toggleVisibility()
    {
        mainContainer.SetActive(!mainContainer.activeSelf);
        hiddenPanel.SetActive(!mainContainer.activeSelf);
    }
}
